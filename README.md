# Whetstone

A tampermonkey script running a collection of frontend hacks for https://www.scabard.com.

Aims to speed up frequently-used functionality in the Scabard interface.

## Current functionality:

### In-Place Thing Edits
Hit Alt+E to enable edit mode and make changes to a page's title, brief summary, description and secrets. 
Hit Alt+S to save changes.
Hit Alt+E again to exit edit mode.

### Create new Thing shortcuts
Press Alt+Q and then tap the first letter of the type of thing you want to make. 

For example to start creating a new Character: press Alt+Q, and then tap C.