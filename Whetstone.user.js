// ==UserScript==
// @name         Scabard Whetstone
// @namespace    https://gitlab.com/Qwertronix/whetstone
// @version      0.1.125
// @description  Attempt to speed up frequently-used functionality in the Scabard interface.
// @author       Qwertronix
// @match        https://www.scabard.com/pbs/campaign/*
// @resource     custom-css https://www.redballoonmelodies.com/wp-content/uploads/2018/10/whetstone-inject.css
// @grant        none
// ==/UserScript==
/*globals $, Mousetrap */

var wrtd = {}
wrtd.editableFields = []
wrtd.lastSaved = {}

function init() {
    // https://www.redballoonmelodies.com/wp-content/uploads/2018/10/whetstone-inject.css

    // kill facebook Share button
    $('.fb-share-button').hide()
    $('.fb_iframe_widget').hide()
    $('#facebook').hide()

    // set up payload identifiers for important in-placed editable elements
    $('#concept-name').attr('payload-id', 'name')
    $('#concept-name').addClass('single-line')
    wrtd.editableFields.push('concept-name')

    // $('#concept-briefSummary').css('font-style', 'italic')
    $('#concept-briefSummary').attr('payload-id', 'briefSummary')
    wrtd.editableFields.push('concept-briefSummary')
    
    $('#description').attr('payload-id', 'description')
    wrtd.editableFields.push('description')
    
    $('#secrets-text').attr('payload-id', 'secrets-text')
    wrtd.editableFields.push('secrets-text')
    
    store.set('w_mode', 'view') // default is view mode, can be 'view' or 'edit'. 

    // TODO: Make this a configurable user setting?
    moreClick(campaignId); // triggers viewing all info on a page
    $('#see-more, #see-less').remove()

    // shouldn't be used yet as some things from /thingedit 
    // aren't inline-editable yet, but for testing...
    $('#main-button-edit').unbind('click').click(function() {
        toggleEditMode();
    });

}

function transformIntoEditableElement(elemid) {
    //test
    $('#'+elemid).attr('contenteditable', 'true')
    var oldborder = $('#'+elemid).css('border')
    if (typeof oldborder !== typeof undefined && oldborder !== false) {
        store.set('oldborder-'+elemid, -1)
    } else {
        store.set('oldborder-'+elemid, oldborder)
    }

    $('#'+elemid).css('border', '2px')
}

function transformIntoNonEditableElement(elemid) {
    //test
    $('#'+elemid).attr('contenteditable', 'false')
    var oldborder = store.get('oldborder-'+elemid)
    if (oldborder !== -1) {
        $('#'+elemid).css('border', oldborder)
    }
    $('#'+elemid).removeAttr('border')
}

function setupHotkeys() {
    var altkeybinds = {
        'c': 'character',
        'e': 'event',
        'g': 'group',
        'i': 'item',
        'p': 'place',
        'n': 'note',
        'f': 'folder',
    };
    for (var key in altkeybinds) {
        if (altkeybinds.hasOwnProperty(key)) {
            console.log("Creating keybindings: " + key + " -> " + altkeybinds[key]);
            Mousetrap.bind('alt+q ' + key, makeNavCreateHandler(altkeybinds[key]));
        }
    };
    var page = $('#page-without-gutters')
    Mousetrap.bindGlobal('control+e', makeEditKeyHandler());
    Mousetrap.bindGlobal('alt+e', makeEditKeyHandler());
}

function getCurrentConcept() {
    return type;
}

function getCurrentChangeTargetURL() {
    var pathArray = window.location.pathname.split('/');
    pathArray.splice(0,2);
    var targetpath = 'https://www.scabard.com/' + pathArray.join('/');
    return targetpath;
}

function getJSONBlobby() {
    var objname = ['jsonBlobby'];
    for (part of window.location.pathname.split('/')) {
        objname.push(part);
    }
    objname = objname.join('_');
    // alert(objname)
    return window[objname];
}

function toggleEditMode() {
    console.log('toggling edit mode')
    if (store.get('w_mode') == 'edit') {
        store.set('w_mode', 'view') // default is view mode, can be 'view' or 'edit'. 
        endEditMode()
    } else {
        store.set('w_mode', 'edit') // default is view mode, can be 'view' or 'edit'. 
        startEditMode()
    }
}

function startEditMode() {
    for (fieldid of wrtd.editableFields) {
        console.log('Making editable: ' + fieldid)
        var elemjq = $('#'+fieldid);
        wrtd.lastSaved[fieldid] = elemjq.html()
        // elemjq.css('background-color', $.Color(elemjq, 'background-color').green(255));
        // elemjq.animate({ 'background-color': $.Color({alpha:1.0, green:255}) }, {duration: 50, complete: function() {
            // elemjq.animate({ 'background-color': $.Color({alpha:0.0, green:0}) }, {duration: 500})
        // }});
        // elemjq.addClass('highlight-for-edit');
        elemjq.effect('highlight', '#0f0', 1600)
        elemjq.attr('contenteditable', 'true')
        elemjq.attr('placeholder', 'Edit text...')
    }
    
    Mousetrap.bindGlobal('control+s', makeSaveKeyHandler());
    Mousetrap.bindGlobal('alt+s', makeSaveKeyHandler());
    console.log('edit mode started!')
}

function saveChanges() {
    console.log('Saving changes...')
    // console.log('Saved ' + fieldid + ': ' + elemjq.html())
    var payload = {_method: 'put', concept: getCurrentConcept()}
    console.log('Saved title value: ' + $('#concept-name').html());
    payload[$('#concept-name').attr('payload-id')] = $('#concept-name').html()
    console.log('Saved subtitle value: ' + $('#concept-briefSummary').html());
    payload[$('#concept-briefSummary').attr('payload-id')] = $('#concept-briefSummary').html()
    console.log('Saved description value: ' + $('#description').html());
    payload[$('#description').attr('payload-id')] = $('#description').html()
    console.log('Saved secrets-text value: ' + $('#secrets-text').html());
    payload[$('#secrets-text').attr('payload-id')] = $('#secrets-text').html()

    wrtd.lastSaved['concept-name'] = $('#concept-name').html()
    wrtd.lastSaved['concept-briefSummary'] = $('#concept-briefSummary').html()
    wrtd.lastSaved['description'] = $('#description').html()
    wrtd.lastSaved['secrets-text'] = $('#secrets-text').html()

    // for (field of store.get('wrtd.editableFields')) {
    //     alert('Saved '+field.attr('payload-id')+' value: ' + field.text());
    //     payload[field.attr('payload-id')] = field.text()
    // }
    console.log('Sending changes...')
    $.post(getCurrentChangeTargetURL(), payload)
    console.log('Updated data sent!')
}

function endEditMode() {
    // for (field of store.get('wrtd.editableFields')) {
    //     field.attr('contenteditable', 'false')
    // }

    var lastval = store.get('last-saved-#concept-name')
    if (wrtd.lastSaved['concept-name'] !== $('#concept-name').html() ||
        wrtd.lastSaved['concept-briefSummary'] !== $('#concept-briefSummary').html() ||
        wrtd.lastSaved['description'] !== $('#description').html() ||
        wrtd.lastSaved['secrets-text'] !== $('#secrets-text').html() ) {
            if (confirm('There are unsaved changes pending, do you want to save '
                +'before exiting edit mode? \n'
                +'(unsaved changes will be lost when leaving or reloading the page)')) { 
                    saveChanges();
            }
    }

    $('#concept-name').attr('contenteditable', 'false')
    $('#concept-briefSummary').attr('contenteditable', 'false')
    $('#description').attr('contenteditable', 'false')
    $('#secrets-text').attr('contenteditable', 'false')

    Mousetrap.unbind('control+s', makeSaveKeyHandler());
    Mousetrap.unbind('alt+s', makeSaveKeyHandler());
    console.log('edit mode ended!')
}


//////////////////////////////////
// USER ACTION HANDLERS

function makeNavCreateHandler(thing) {
    return function(e) {
        if (e.preventDefault) { e.preventDefault(); } else { e.returnValue = false; }
        e.stopPropagation()
        if (!confirm('Create new '+thing+'?')) { return; }
        window.location.href = "/pbs/campaign/" + getCampaignID() + "/" + thing + "/new";
        return false;
    }
}

function makeEditKeyHandler(thing) {
    return function(e) {
        if (e.preventDefault) { e.preventDefault(); } else { e.returnValue = false; }
        e.stopPropagation()
        toggleEditMode();
        return false;
    }
}

function makeSaveKeyHandler(thing) {
    return function(e) {
        if (e.preventDefault) { e.preventDefault(); } else { e.returnValue = false; }
        e.stopPropagation()
        if (store.get('w_mode') == 'edit') {
            saveChanges();
        }
        return false;
    }
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// Entry point / "Main" function lookalike
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

$( document ).ready($.when(
    $.getScript( 'https://cdnjs.cloudflare.com/ajax/libs/loadjs/3.5.4/loadjs.min.js' ),
    $.getScript( "https://cdnjs.cloudflare.com/ajax/libs/store.js/1.3.20/store.min.js" ),
    $.getScript( "https://cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js" ),
    $.Deferred(function( deferred ){
        $( deferred.resolve );
    })
).done(function(){
    loadjs('https://cdnjs.cloudflare.com/ajax/libs/mousetrap/1.6.2/mousetrap.min.js', 'Mousetrap');
    loadjs('https://www.redballoonmelodies.com/wp-content/uploads/2018/10/mousetrap-global-bind.min_.js', 'MousetrapGlobal');

    loadjs.ready(['Mousetrap', 'MousetrapGlobal'], function() {
        init();
        setupHotkeys();
    });
}));

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// Helper functions from external sources
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

//////////////////////////////////
// credit for below: 

//////////////////////////////////

